import React from 'react'

function Main() {
    return (
        <>
            <div className="carousel-inner">
                <div className="carousel-item active">
                    <img src="https://www.hostgator.com/blog/wp-content/uploads/2020/08/Types-of-eCommerce-Websites-1024x538.jpg" className="d-block w-100" alt="..." height={"650"} />
                    <div className="carousel-caption d-none d-md-block " style={{color: 'black'}}>
                        <h2>New Arrival</h2>
                        <h4>Fill your basket with latest finds.</h4>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Main
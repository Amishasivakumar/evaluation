import { BrowserRouter, Route, Routes } from "react-router-dom"
import Home from "./Pages/Home"
import Products from "./Pages/Products"
import MoreDetails from "./Pages/MoreDetails"
import Cart from "./Pages/Cart"

function App() {

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products" element={< Products />} />
          <Route path="/moredetails/:id" element={<MoreDetails />} />
          <Route path="/cart" element={<Cart />} />
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App

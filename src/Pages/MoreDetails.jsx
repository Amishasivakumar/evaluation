import React, { useEffect, useState } from 'react'
import Navbar from '../Components/Navbar'
import { Link, useParams } from "react-router-dom"
import { useDispatch } from 'react-redux';
import { addItem } from '../Redux/CartSlice';
import Footer from '../Components/Footer';

function MoreDetails() {
    const { id } = useParams();
    const [product, setProduct] = useState(null);

    useEffect(() => {
        fetch(`https://dummyjson.com/products/${id}`)
            .then((response) => response.json())
            .then((data) => setProduct(data));
    }, [id]);

    const dispatch = useDispatch()
    const handleAddToCart = () => {
        dispatch(addItem(product));
        alert(`Added ${product.title} to cart!`);
    };

    if (!product) {
        return <div>Loading...</div>;
    }

    return (
        <>
            <Navbar /> <br />
            <center>
                <div className="container row" key={product.id}>
                    <h1>{product.title}</h1>
                    <div className="col-md-6 col-sm-12 py-3 ">
                        <img
                            className="img-fluid"
                            src={product.thumbnail}
                            alt={product.title}
                            width="200px"
                            height="200px"
                        />
                    </div>
                    <div className="col-md-6 col-md-6 py-5 text-start">
                        <p>{product.description}</p>
                        <p>
                            {product.rating}   <i className="bi bi-star-fill"></i>
                        </p>
                        <h3 className="display-6  my-4">${product.price}</h3>
                        <p className="card-text">
                            -{product.discountPercentage}%
                        </p>
                        <button className="btn btn-dark m-1" onClick={handleAddToCart}>
                            Add to Cart
                        </button>
                        <Link className="btn btn-dark m-1" to={"/cart"}>
                            Go to Cart
                        </Link>
                    </div>
                </div>
            </center> <br />
            <Footer />

        </>
    )
}

export default MoreDetails;
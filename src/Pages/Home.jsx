import React from 'react'
import Navbar from '../Components/Navbar'
import Main from '../Components/Main'
import Footer from '../Components/Footer'

function Home() {
    return (
        <>
            <Navbar />
            <Main />
            <Footer/>
        </>
    )
}

export default Home
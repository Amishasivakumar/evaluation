import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Navbar from "../Components/Navbar"
import Footer from "../Components/Footer";

const Products = () => {
    const [products, setProducts] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        fetch(' https://dummyjson.com/products')
            .then(response => response.json())
            .then(data => {
                setProducts(data.products);
            });
    }, []);
    const filteredProducts = products.filter(product =>
        product.title.toLowerCase().includes(searchTerm.toLowerCase())
    );
    console.log(products);

    return (
        <>
            <Navbar />
            <h1 className="text-center">Products</h1>
            <div className="container my-5">
                <input
                    type="text"
                    placeholder="Search products..."
                    className="form-control border border-black mb-4"
                    onChange={(e) => setSearchTerm(e.target.value)}
                />
                <div className="row">
                    {filteredProducts.map(product => (
                        <div className="col-md-4 mb-4" key={product.id}>
                            <div className="card mb-4 shadow-sm">
                                <img
                                    className="card-img- p-3"
                                    src={product.thumbnail}
                                    alt={product.title}
                                    height={"300px"}
                                />
                            </div>
                            <div className="card-body">
                                <h5 className="card-title" >{product.title}</h5>
                                <h3 className="my-3">${product.price}</h3>
                                <Link to={`/moredetails/${product.id}`} className="btn btn-dark" >
                                    More Details
                                </Link>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <Footer />
        </>
    );
};

export default Products;

